# Quake For Newbies Discord

Invite link: https://discord.q4n.eu/

We play AFPS games, and less experienced players are welcome here! We're happy to help, or just have fun playing together. Or, you know, both.

## Quake Live servers

* EU Elo 1100 max: `/connect eu.quakenewbies.com` ([Steam link](steam://connect/eu.quakenewbies.com:27960))
* EU Party: `/connect eu.quakenewbies.com:27961` ([Steam link](steam://connect/eu.quakenewbies.com:27961))
* EU 1000-1500 Elo: `/connect eu2.quakenewbies.com` ([Steam link](steam://connect/eu2.quakenewbies.com:27960))
* NA East Elo 1100 max: `/connect na.quakenewbies.com` ([Steam link](steam://connect/na.quakenewbies.com:27960))

[QL EU map list](https://docs.google.com/spreadsheets/d/1F91NW8BcnyHexbckaQiw2kpF1TJNvkS-cf5Bn5t_r5g/edit?usp=sharing)

Also some "less Quake Live" servers:

* CPMA: `/connect df.quakenewbies.com:27961`
* DeFRaG: `/connect df.quakenewbies.com`
* Quake 4 duel: `/connect df.quakenewbies.com:28004`
* nQuake: `/connect df.quakenewbies.com:28501`

The servers may be password-protected occasionally, join the Discord to get access.

## Helpful links from the community

* https://docs.google.com/spreadsheets/d/1kgiaOwSvBUOYbc6LAL1cj4JGL_ytWZat5oqcJGMbG_M/edit#gid=0

## Demo guides

* [Demo marking](/demo_guides/demo_marking/README.md)
* [Demo rendering](/demo_guides/demo_rendering/README.md)
