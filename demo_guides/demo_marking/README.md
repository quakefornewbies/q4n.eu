# Demo Marking

## The Concept

There is a neat piece of software called UDT that allows you to cut QL and Q3 demos into little snippets. It has a feature where if it detects a specified chat message, it can automatically cut a demo from that bit of the game.

The idea is that you have a chatbind with some unique words that the software can recognize. Whenever something cool happens, you press your chatbind key, everyone will see that you have considered the frag or whatever happened worthy.

## Making a chat bind

My chatbind goes something like `bind MOUSE5 say "burt, please mArKdEmO"`, but it would be no good if you would just copy that. You should think of some message, like Olmos *"Marker den frækkert"*.

Then you do a bind like `bind x say "message"`. Everytime you press x now there will be a message in the chat. Just make sure you're recording a demo and mark your shit then you're set for the next step.

## Using UDT

First, you'll need to get UberDemoTools, the tool that we will be using. It can either be obtained from [mYt's official website](https://myt.playmorepromode.com/udt/redirections/windows_gui_x64.html) or, just as a backup, from [froschgrosch.de](https://froschgrosch.de/files/udt/udt_con_1.3.1_x64.zip). If you are running on 32bit there are appropiate versions for that, those can be obtained [here](https://myt.playmorepromode.com/udt/redirections/windows_gui_x86.html) or [here](https://froschgrosch.de/files/udt/udt_gui_0.7.2_dll_1.3.1_x86.zip) respectively.

### First steps with the application

Tumer shared this wonderful screenshot with an error message.

![An error message that says that "giant.pourri.ch" can't be reached.](images/error.png)

This is because the built-in updater doesn't work anymore and nobody has bothered fixing it. It can be taken care of by going to *Settings > Start-up Updates* and disabling that option.

![A screenshot showing where to find the option.](images/settings.png)

### Creating chat patterns

First you will go to *Patterns > Chat* and create a new chat rule. You can change the parameters but sticking with the preset ones is fine.

![A screenshot showing the popup window for creating chat patterns.](images/chatrule.png)

After your pattern is created, you can import some demos into UDT. Of course, you're not restricted to only use the chat patterns, there are a bunch of other patterns as well that you can mess about with, I just personally am not using those, so feel free to let others know your findings.

### Importing demos

Make sure you're in the *Patterns > Chat* or in *Pattern Search* menu. Either drag-and-drop into the demo list thingy, or use the file option up top to import some.

### Cutting

The next step is cutting. When you click the *Cut!* button, there will be a window where you can select the offset before and after the mark. You can tweak those for individual frags if it is too short. Default is 10/10, but doing some longer shouldn't do any harm.

After that, UDT will do its thing and put all cut demos next to the demo they originated from. So if you had a demo `foo.dm_91` in a folder, there will be some demos named `foo_CUT_CHAT_blah.dm_91` in the folder now. You can rename those with some scripts if that is your kind of thing, or use an application like [Bulk Rename Utility](https://www.bulkrenameutility.co.uk/).

## What to do after Cutting

When you eventually have all your demos cut, it is time to jump into the game and check them all. Watch each snippet and decide what to keep and what not to. Over time, you can amass a collection of cool frags.

A little insight: my friends and me apparently had over 2200 clips available for RA3newbs2.

![A screenshot showing the popup window for creating chat patterns.](images/demolistings.png)

So if you went through all of your demos, you can organize them in some folders and when it is time, send the interesting ones to the folks doing fragmovies, or of course, do one yourself :]
