# Demo rendering in Quake Live

## About the concept

So, the preferred way to convert demos to videos is to use some kind of rendering instead of just capturing the QL / Wolfcam / Q3 window in OBS. In quake3e, it is supported to directly pipe the video to the ffmpeg application, which actually compresses the video with your codec of choice. In most other cases, you will have to create a temporary file and compress that with ffmpeg (or other tools if you want).

## Software requirements

You will probably need ffmpeg to get the videos into an usable format. You can grab it from [the official website](http://ffmpeg.org/download.html).

## Rendering in WolfcamQL

Grab wolfcam [here](https://github.com/brugal/wolfcamql/releases/latest).

### Some cvars

`cl_freezeDemoPauseVideoRecording 1` to pause recording of video/screen-shots while paused.

`cl_aviCodec "huffyuv"`
The options for *cl_aviCodec* are *uncompressed*, *huffyuv* and *mjpeg*.
I recommend *huffyuv* as it somewhat reduces temporary file size and doesn't take much longer to render in wolfcam.
You can use *mjpeg* if you don't have much disk space for temporary files, but it will slow down rendering significantly. It will also induce slight jpeg artifacts, although those won't be so noticeable in the end result.

`cl_aviFrameRate 60`
Set the desired output video framerate here.

### Capturing in wolfcam

The actual command for capturing in wolfcam is `/video tga wav name render_temp`
You can replace *render_temp* with *:demoname*, and wolfcam will use the demo name as the capture filename. Or any other name, and it will render to that. **This guide assumes that it is set to render_temp**.

### Encoding

After capturing, the captured files normally are in `%appdata%/Wolfcamql/wolfcam-ql/videos` (just paste that into your windows explorer address).
After that, we will encode the .avi files to something more reasonable in size. We will use ffmpeg for that.

#### Creating a batch file for convenience

Navigate to the output folder
Right-click/New/Text file
name it render.txt
open it in editor

Now, paste this into the file:

``` Windows Batch file
@echo off
ffmpeg -hide_banner -i render_temp.avi -c:v libx264 -crf:v 21 -preset:v medium -tune:v film -c:a aac -b:a 384k  "render_output.mp4"

echo.
echo Rendering finished.
pause
```

Save as
Filetype "all files (\*.\*)"
Name it render.bat
Double click and it should start rendering.

#### Information for a few parameters

`-preset:v medium` sets the "speed" of the encoder. It is a tradeoff between compression strength and encoding speed. Sensible presets are `veryfast` `faster` `fast` `medium` `slow` `slower` `veryslow`.

`-crf:v` sets the visual quality of the video (lower means better). Just try to see what is good enough for you, as higher will increase file size.

Visit [the ffmpeg wiki](https://trac.ffmpeg.org/#Encoding) for more information on encoding.

When rendering is complete, it will say "Rendering finished.". After that, you can close that window and look at your *render_output.mp4* file. It should now be ready to be used however you like.
